// SilicaAndPina
// NoGameUpdate

#include <taihen.h>
#include <vitasdk.h>
#define SCE_GAME_UPDATE_APP_VER_SIZE		6	/* "XX.XX" + NULL */

static SceUID hook;
static tai_hook_ref_t ref_hook;

int ret;

typedef struct SceGameUpdateResult {
	unsigned int	size;					/* sizeof(SceGameUpdateResult) */
	SceBool			patchExist;
	char			appVer[SCE_GAME_UPDATE_APP_VER_SIZE];
	char			padding[2];
} SceGameUpdateResult;


int sceGameUpdateRun_patched(SceGameUpdateResult *result) {
  ret = TAI_CONTINUE(int,&ref_hook,result);
  result->patchExist = 0;
  return ret;
}



void _start() __attribute__ ((weak, alias ("module_start")));

int module_start(SceSize argc, const void *args) {

  hook = taiHookFunctionImport(&ref_hook,
                                TAI_MAIN_MODULE,
                                TAI_ANY_LIBRARY,
                                0x3C616238, // sceGameUpdateRun
                                sceGameUpdateRun_patched);

  return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args) {

  // release hooks
  if (hook >= 0) taiHookRelease(hook, ref_hook);


  return SCE_KERNEL_STOP_SUCCESS;
}
